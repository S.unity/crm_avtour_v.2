import api from "../API/api";
import {useLogLead} from "./useLogLead";

export function useUpdateLead (data) {
  const {_id, key, value, index, log} = data
  log && useLogLead(_id, log)
  return api.updateLead({_id, key, value, index})
}
