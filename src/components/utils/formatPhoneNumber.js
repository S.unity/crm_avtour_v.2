export function formatPhoneNumber(phoneNumber) {
  // Удаляем все символы, кроме цифр
  const cleanedNumber = phoneNumber.replace(/\D/g, '');

  // Если длина номера равна 10 цифрам, добавляем "+7" в начало
  if (cleanedNumber.length === 10) {
    return '+7' + cleanedNumber;
  } else {
    // В остальных случаях возвращаем "+7" и первые 10 цифр
    return '+7' + cleanedNumber.slice(-10);
  }
}
