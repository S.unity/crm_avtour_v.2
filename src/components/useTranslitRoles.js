export default function useTranslitRoles(opt) {
    switch (opt) {
        case 'ADMIN':
            return 'Админ';
        case 'suHOS':
            return 'СуперРОП';
        case 'HOS':
            return 'РОП';
        case 'SALESMAN':
            return 'Менеджер';
        case 'LAWYER':
            return 'Юрист';
        case 'LEADSMAN':
            return 'Лидогенератор';
        case 'FINANCIER':
            return 'Финансист';
        case 'USER':
            return 'Пользователь';
        default:
            return '';
    }
}
