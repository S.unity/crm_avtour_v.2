export function useCompareDate(obj) {
    const {date, time} = obj
    function compareDateTime(dateTime) {
        const targetDateTime = new Date(dateTime); // Преобразуем переданную дату в объект Date

        // Получаем текущую дату и время
        const currentDateTime = new Date();

        // Сравниваем текущую дату и время с переданной датой и временем
        if (currentDateTime > targetDateTime) {
            return false; // Текущее время больше, чем переданное время
        } else {
            return true; // Текущее время меньше или равно переданному времени
        }
    }
    return compareDateTime(date)

    // function compareDates(inputDate) {
    //     const currentDate = new Date().setHours(0, 0, 0, 0);
    //     const parsedDate = new Date(inputDate).setHours(0, 0, 0, 0);
    //
    //     return parsedDate > currentDate;
    // }
    //
    // function compareTimes(inputTime) {
    //     if (inputTime) {
    //         const currentTime = new Date().getTime();
    //         const parsedTime = new Date().setHours(
    //             parseInt(inputTime.split(':')[0]),
    //             parseInt(inputTime.split(':')[1]),
    //             0, 0
    //         );
    //         return parsedTime > currentTime;
    //     }
    // }
    //
    //
    // if (date) return compareDates(date)
    // else return compareTimes(time)
}