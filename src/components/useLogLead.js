import api from "../API/api";
import store from "../vuex/store";

export async function useLogLead (lead_id, log, data = []) {
  const user = store.state.user;
  await api.logLead({
    lead_id,
    user: {username: user.username, _id: user._id},
    log: log.length && log || ''
  }).then(res => data.value = [...res.data.log.reverse()])
}
