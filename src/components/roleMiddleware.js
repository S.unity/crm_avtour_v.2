import store from '../vuex/store'
export function roleMiddleware (roles) {
    const role = store.state.token && store.state.user.roles[0] || ['user']
    return !!roles.includes(role);
}