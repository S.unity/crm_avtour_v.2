import { io } from 'socket.io-client';
import store from '../vuex/store'
const token = store.getters.token || localStorage.token

const URL = 'https://crm1-api.technik-crm.ru/'
const socket = io(URL, {
  withCredentials: true,
  extraHeaders: {
    "my-custom-header": `Bearer ${token}`
  }
});

let isStreamInitialized = false;

export function stream(key, data, _id='') {
  if (!isStreamInitialized) {
    socket.on("connect", () => {
      return process.env.NODE_ENV === "production" ? console.log('connected') : console.log('disconnected');
    });
    isStreamInitialized = true;
  }

  if (key === 'user online') {
    socket.on(key, (el) => {
      store.state.online_count = el
      return data.value = el
    })
    return true
  }
  key === 'add lead' && socket.on(key, (el) => {
    data.unshift(JSON.parse(el))
  });
  key === 'change lead' && socket.on(key, (el) => {
    const {_id, key, value, index} = JSON.parse(el)
    const indexRow = data.findIndex(item => item._id === _id)
    if (key !== 'message-secured' && key !== 'message' && key !== 'fields') data[indexRow][key] = value
  });
  key === 'lead message' && socket.on(key, (el) => {
    const obj = JSON.parse(el)
    obj._id === _id && data.unshift(obj.message)
  });

  key === 'leads delete' && socket.on(key, (el) => {
    const arrDeletedLead = JSON.parse(el)
    arrDeletedLead.filter((el) => {
      data.filter((row, index) => {
        el === row._id && data.splice(index, 1)
        return true
      })
    })
  });

  socket.on(key, (el) => {
    const obj = JSON.parse(el)
    if (_id && obj._id === _id) {
      return data.push(obj.message)
    } else return data.push(obj)
  });

}
