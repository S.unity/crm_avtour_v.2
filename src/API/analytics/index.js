import apiAddress from "../address";
import store from '../../vuex/store'

export default  {
    getAnalytics (data) {
        return apiAddress().post('analytics', data, {
            headers: {
                // eslint-disable-next-line
                'Authorization': `Bearer ${store.state.token}`
            },
        })
    },
    getDept (data) {
        return apiAddress().post('analytics/dept', data, {
            headers: {
                // eslint-disable-next-line
                'Authorization': `Bearer ${store.state.token}`
            },
        })
    },
    getDeptOne (id) {
        return apiAddress().post(`analytics/dept-one`, id, {
            headers: {
                // eslint-disable-next-line
                'Authorization': `Bearer ${store.state.token}`
            },
        })
    }
}