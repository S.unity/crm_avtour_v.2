import axios from 'axios'
export default () => {
  return axios.create({
    // baseURL: 'http://192.168.1.152:5000/'
    // baseURL: 'http://localhost:7000/' // nestjs
    // baseURL: 'http://localhost:5000/'
    baseURL: 'https://crm1-api.technik-crm.ru/'
  })
}
