import { createApp } from 'vue'
import App from './App.vue'
import router from '../src/router';
import clickOutside from '../../dash_wb/src/utils/click-outside';
import store from './vuex/store';
import { Quasar } from 'quasar'
// Import icon libraries
import '@quasar/extras/material-icons/material-icons.css'
// Import Quasar css
import 'quasar/src/css/index.sass'
import './assets/style.scss'


// запрет на вход не авторизованному пользователю
router.beforeEach(async (to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    /*
    этот путь требует авторизации, проверяем залогинен ли
    пользователь, и если нет, запрещаем доступ к странице
     */
    if (store.state.token && localStorage.token && store.state.user.status) {
      next()
    } else {
      // Проверяем, не является ли текущий путь страницей 404
      if (to.name === 'login') {
        next(false)
      } else {
        next({
          path: '/login'
        })
      }
    }
  } else {
    next() // всегда так или иначе нужно вызвать next()!
  }
})
if (localStorage.token) {
  store.state.token = localStorage.token
  store.state.user = JSON.parse(localStorage.user)
}


createApp(App)
  .use(store)
  .use(Quasar, {
    plugins: {}, // import Quasar plugins and add here
  })
  .use(router)
  .directive('click-outside', clickOutside)
  .mount('#app')
