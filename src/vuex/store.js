import { createStore } from "vuex";

export default createStore({
  state: {
    user: {},
    token: '',
    search: [],
    searchCat: [],
    hierarchy: [],
    online_count: 0,
    darkStyle: false,
    error: []
  },
  mutations: {
    SEARCH_CLEAR (state) {
      state.search = []
    },
    DARK_STYLE (state) {
      state.darkStyle = !state.darkStyle
    },
    ERROR (state, data) {
      state.error.push(data)
    },
    SPLICE_ERROR (state) {
      state.error.splice(0, state.error.length)
    },
  },
  actions: {
    SPLICE_ERROR: ({commit}) => commit('SPLICE_ERROR')
  },
  getters: {
    search (state) {
      return state.search
    },
    token (state) {
      return state.token
    },
    isDark (state) {
      return !!state.darkStyle
    },
    isAuth (state) {
      return !!state.token
    },
  }
});
