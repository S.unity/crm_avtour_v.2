import { createRouter, createMemoryHistory, createWebHistory } from 'vue-router';

const isServer = typeof window === 'undefined';
const history = isServer ? createMemoryHistory() : createWebHistory();
const routes = [
    {
        path: '/',
        name: 'Index',
        component: ()=> import('../views/Main/Index.vue'),
        meta: {
            requiresAuth: true,
        }
    },
    {
        path: '/documents',
        name: 'Documents',
        component: ()=> import('../views/Documents.vue'),
        meta: {
            requiresAuth: true,
        }
    },
    {
        path: '/analytics',
        name: 'Analytics',
        component: ()=> import('../views/Analytics.vue'),
        meta: {
            requiresAuth: true,
        }
    },
    {
        path: '/lead/:id',
        name: 'Lead',
        component: ()=> import('../views/Lead/Lead.vue'),
        props: true,
        meta: {
            // requiresAuth: true,
        }
    },
    {
        path: '/profile',
        name: 'Profile',
        component: ()=> import('../views/user/Profile.vue'),
        meta: {
            requiresAuth: true,
        }
    },
    {
        path: '/analytics',
        name: 'Analytics',
        component: ()=> import('../views/Analytics/index.vue'),
        meta: {
            requiresAuth: true,
        }
    },
    {
        path: '/task',
        name: 'Task',
        component: ()=> import('../views/Tasks/Task.vue'),
        children: [
        ],
        meta: {
            requiresAuth: true,
        }
    },
    {
        path: '/task/users',
        name: 'taskUsers',
        component: ()=> import('../views/Tasks/Users.vue'),
        meta: {
            requiresAuth: true,
        }
    },
    {
        path: '/login',
        name: 'Login',
        component: ()=> import('../views/Auth.vue'),
    },
    {
        path: '/settings',
        name: 'Settings',
        component: ()=> import('../views/settings/Index.vue'),
        meta: {
            requiresAuth: true,
        }
    },
    {
        path: '/notice',
        name: 'Notice',
        component: ()=> import('../views/Notice/index.vue'),
        meta: {
            requiresAuth: true,
        }
    },
    {
        path: '/404',
        name: '404',
        component: ()=> import('../views/404.vue'),
    },
];
const router = createRouter({
    history,
    routes,
});

export default router;
