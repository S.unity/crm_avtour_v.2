// export default function formattedDate(inputDate, includeTime) {
//   const date = new Date(inputDate);
//   const day = String(date.getUTCDate()).padStart(2, '0');
//   const month = String(date.getUTCMonth() + 1).padStart(2, '0');
//   const year = String(date.getUTCFullYear()).slice(-2);
//
//   if (includeTime) {
//     const hours = String(date.getHours()).padStart(2, '0');
//     const minutes = String(date.getMinutes()).padStart(2, '0');
//     return `${day}/${month}/${year} ${hours}:${minutes}`;
//   } else {
//     return `${day}/${month}/${year}`;
//   }
// }
export default function formattedDate(inputDate, includeTime) {
  // Проанализируйте введенную дату
  const date = typeof inputDate === 'number' ? new Date(inputDate) : new Date(Date.parse(inputDate));

  // Извлекать день, месяц и год из объекта date по местному времени
  const day = String(date.getDate()).padStart(2, '0');
  const month = String(date.getMonth() + 1).padStart(2, '0');
  const year = String(date.getFullYear()).slice(-2);

  if (includeTime) {
    // Извлеките часы и минуты из объекта даты по местному времени
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');
    return `${day}/${month}/${year} ${hours}:${minutes}`;
  } else {
    return `${day}/${month}/${year}`;
  }
}